import telepot
from telepot.loop import MessageLoop
import time
import json
import os
import sys
import subprocess
import requests


"Read configuration info from file: Bot token, node_name and password"
while True:
    try:
        with open('/home/Iota_node_manager/config.txt') as configFile:
            configfileData = json.load(configFile)
        print('Opening file...')
        TOKEN = configfileData['bot_token']
        PASSWORD = configfileData['password']
        node_name = configfileData['node_name']
        admin_name = configfileData['admin_username']
        node_local_address = configfileData['node_local_address']
        global_lmsi_address = configfileData['global_lmsi_address']
        print("Initialization completed!")
        break
    except:
        print("'config.txt' file not found...\n")
        print("Please create the file and place it in script folder...\n")
        input("After file creation, press enter to retry...\n\n");

"Initialize bot"
myBot = telepot.Bot(TOKEN)

"Testing the bot"
print(myBot.getMe())

"Define the message listener function"
def messageListener(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    user_name = msg['chat']['username']
    user_id = msg['chat']['id']
    if (content_type == 'text') & (user_name == admin_name):
        if msg['text'] == '/start':
            myBot.sendMessage(user_id, 'Ready to execute specific commands! Check out the commands list!')
        if msg['text'] == '/check_bot':
            myBot.sendMessage(user_id, 'I\'m active and waiting for commands!')
        if msg['text'] == '/restart_node':
            myBot.sendMessage(user_id, 'Your node ' + node_name + ' will be restarted in 3 seconds...')
            time.sleep(1)
            myBot.sendMessage(user_id, '3')
            time.sleep(1)
            myBot.sendMessage(user_id, '2')
            time.sleep(1)
            myBot.sendMessage(user_id, '1')
            time.sleep(1)
            cmd = '/bin/systemctl restart iri'
            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            proc.communicate()
            myBot.sendMessage(user_id, 'Your node ' + node_name + ' has been restarted.')
        if msg['text'] == '/start_node':
            myBot.sendMessage(user_id, 'Your node ' + node_name + ' will be started in 3 seconds...')
            time.sleep(1)
            myBot.sendMessage(user_id, '3')
            time.sleep(1)
            myBot.sendMessage(user_id, '2')
            time.sleep(1)
            myBot.sendMessage(user_id, '1')
            time.sleep(1)
            cmd = '/bin/systemctl start iri'
            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            proc.communicate()
            myBot.sendMessage(user_id, 'Your node ' + node_name + ' has been started.')
        if msg['text'] == '/stop_node':
            myBot.sendMessage(user_id, 'Your node ' + node_name + ' will be stopped in 3 seconds...')
            time.sleep(1)
            myBot.sendMessage(user_id, '3')
            time.sleep(1)
            myBot.sendMessage(user_id, '2')
            time.sleep(1)
            myBot.sendMessage(user_id, '1')
            time.sleep(1)
            cmd = '/bin/systemctl stop iri'
            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            proc.communicate()
            myBot.sendMessage(user_id, 'Your node ' + node_name + ' has been stopped.')
        if msg['text'] == '/check_sync':
            parameters = {
                'command': 'getNodeInfo',
                'X-IOTA-API-Version': '1'
            }
            headers = {
                'X-IOTA-API-Version': '1',
                'Content-Type': 'application/json'
            }

            "Call global info and localNode to collect indexes info"
            try:
                nodeInfo = requests.post(url=node_local_address, data=json.dumps(parameters), headers=headers)
                globalMilestoneInfo = requests.get(url=global_lmsi_address, headers={'Content-Type': 'application/json'})
            except:
                myBot.sendMessage(user_id, 'I\'m sorry, i can\'t connect to local node or to global index website')
                nodeInfo = 0
                globalMilestoneInfo = 0

            "Post message with required info"
            if nodeInfo.status_code == 200 & globalMilestoneInfo.status_code == 200:
                nodeInfoJson = json.loads(nodeInfo.text)
                globalMilestoneInfoJson = json.loads(globalMilestoneInfo.text)
                messageToSend = ('Global LMI: ' + str(globalMilestoneInfoJson['latestMilestoneIndex']) + "\n" +
                                 'Local LMI: ' + str(nodeInfoJson['latestMilestoneIndex']) + "\n" +
                                 'Local LSMI: ' + str(nodeInfoJson['latestSolidSubtangleMilestoneIndex']))
                myBot.sendMessage(user_id, messageToSend)
            else:
                myBot.sendMessage(user_id, 'I\'m sorry, i can\'t reach the required data')

    else:
        myBot.sendMessage(user_id, 'Your message is not a correct command or your USERNAME is not allowed to send commands')
        myBot.sendMessage(user_id, 'Please contact the BOT administrator')



"Activate listening loop"
MessageLoop(myBot, messageListener).run_as_thread()
print('Listening ...')

"Keep the program running."
while 1:
    time.sleep(3)
